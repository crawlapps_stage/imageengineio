<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CustomDomain;
use App\Models\User;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getDomain(Request $request){
        try{
            $shop = $request->shop;
            $user = User::where('name', $shop)->first();

            if( $user ){
                $db_domain = CustomDomain::select('domain')->where('user_id', $user->id)->first();

                return response()->json(['data' => $db_domain, 'isSuccess' => true], 200);
            }else{
                return response()->json(['data' => 'user not found...', 'isSuccess' => false], 422);
            }
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }
}
