<?php

namespace App\Http\Controllers\Domain;

use App\Http\Controllers\Controller;
use App\Models\CustomDomain;
use App\Models\Token;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DomainController extends Controller
{
    /**
     * Return component
     * @param  Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function component(Request $request){
        try{
            $pathInfo = $request->getPathInfo();
            return view('layouts.app', compact('pathInfo'));
        }catch ( \Exception $e ){
            dd($e);
        }
    }

    /**
     * Get domain
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        try{
            $user = Auth::user();
            $domain = CustomDomain::select('id', 'domain')->where('user_id', $user->id)->first();
            if(!$domain){
                $domain['id'] = '';
                $domain['domain'] = '';
            }
            return response()->json(['data' => $domain, 'isSuccess' => true], 200);
        }catch ( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    /**
     * Store domain
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request){
        try{
            $user = Auth::user();

            $data = $request->data;

            DB::beginTransaction();

            $domain = ( $data['id'] != '' ) ? CustomDomain::where('id', $data['id'])->where('user_id', $user->id)->first() : new CustomDomain;
            $domain->user_id = $user->id;
            $domain->domain = $data['domain'];
            $domain->save();

            $theme_id = getThemeH();
            addSnippetH($user->id, $data['domain'], $theme_id);
            DB::commit();
            $msg = 'Saved!';
            return response()->json(['data' => $msg, 'isSuccess' => true], 200);
        }catch ( \Exception $e ){
            DB::rollBack();
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }
}
