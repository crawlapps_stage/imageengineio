<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Models\CustomDomain;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    public function index( Request $request){
        try{
            $shop = Auth::user();
            $parameter['fields'] = 'id,name,role';
            $sh_themes = $shop->api()->rest('GET', 'admin/themes.json', $parameter);

            $theme = [];
            if (!$sh_themes['errors']) {
                $themes = $sh_themes['body']->container['themes'];
                foreach ($themes as $key => $val) {
                    $theme[$key]['value'] = (string)$val['id'];
                    $theme[$key]['label'] = ( $val['role'] == 'main' ) ? $val['name'] . ' ( published )' : $val['name'];

                    if( $val['role'] == 'main' ){
                        $curr_theme['value'] = $val['id'];
                        $curr_theme['label'] = $val['name'];
                    }
                }
            }

            $data['theme'] = $theme;
            $data['current'] = $curr_theme;
            return response()->json(['data' => $data, 'isSuccess' => true], 200);
        }catch ( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    /**
     * Store domain
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request){
        try{
            $user = Auth::user();

            $theme_id = $request->data;

            DB::beginTransaction();
            $domain = CustomDomain::where('user_id', $user->id)->first();

            if( $domain ){
                addSnippetH($user->id, $domain->domain , $theme_id);
                return response()->json(['data' => 'Snippet created', 'isSuccess' => true], 200);
            }else{
                return response()->json(['data' => 'Domain not found', 'isSuccess' => false], 422);
            }

        }catch ( \Exception $e ){
            DB::rollBack();
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }
}
