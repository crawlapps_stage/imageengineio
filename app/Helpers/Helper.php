<?php

use App\Models\User;
use Illuminate\Support\Facades\Auth;

if (!function_exists('addSnippetH')) {

    /**
    * @return mixed
    *
    */
    function addSnippetH($user_id, $domain, $theme_id)
    {
        try {
            $user = Auth::user();

            $type = 'add';
            if ($type == 'add') {
                $v = getsnippetCode($domain);
                $value = <<<EOF
<script type="text/javascript">
        $v
</script>
EOF;
            }
            $parameter['asset']['key'] = 'snippets/image-engine.liquid';
            $parameter['asset']['value'] = $value;
            $asset = $user->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);

            updateThemeLiquidH('image-engine', $theme_id, $user_id);
        } catch (\Exception $e) {
            \Log::info('-----------------------ERROR :: addSnippet -----------------------');
            \Log::info(json_encode($e));
        }
    }
}

//update theme.liquid
if (!function_exists('updateThemeLiquidH')) {
    /**
     * @param $snippet_name
     * @param $theme_id
     */
    function updateThemeLiquidH($snippet_name, $theme_id, $user_id){
        try {
            \Log::info('-----------------------START :: updateThemeLiquidH -----------------------');
            $user = User::find($user_id);

            $asset = getLiquidAssetH($theme_id, $user_id, 'layout/theme.liquid');
            if ($asset != '') {
                // add before </body>
                if (!strpos($asset, "{% render '$snippet_name' %}")) {
                    $asset = str_replace('</body>', "{% render '$snippet_name' %}</body>", $asset);
                }

                $prelink = '<link rel="preconnect" href="' .env('APP_URL') .'" >';
                if (!strpos($asset, $prelink)) {
                    $asset = str_replace('</head>', "$prelink</head>", $asset);
                }

                $parameter['asset']['key'] = 'layout/theme.liquid';
                $parameter['asset']['value'] = $asset;
                $result = $user->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);
            }
        } catch (\Exception $e) {
            \Log::info('-----------------------ERROR :: updateThemeLiquidH -----------------------');
            \Log::info(json_encode($e));
        }
    }
}

if (!function_exists('getLiquidAssetH')) {
    /**
     * @return string
     */
    function getLiquidAssetH($theme_id, $user_id, $file)
    {
        $user = User::find($user_id);

        $asset = $user->api()->rest('GET', 'admin/themes/'.$theme_id.'/assets.json',
            ["asset[key]" => $file]);

        return (@$asset['body']->container['asset']['value']) ? $asset['body']->container['asset']['value'] : '';
    }
}

//get published theme
if (!function_exists('getThemeH')) {
    /**
     * @return false|mixed
     */
    function getThemeH()
    {
        try {
            $shop = \Auth::user();
            $parameter['role'] = 'main';
            $result = $shop->api()->rest('GET', '/admin/api/'.env('SHOPIFY_API_VERSION').'/themes.json', $parameter);
//            \Log::info($result);

            if (!$result['errors']) {
                return $result['body']->container['themes'][0]['id'];
            } else {
                return false;
            }
        } catch (\Exception $e) {
            dd($e);
        }
    }
}

// getsnippetCode
if (!function_exists('getsnippetCode')) {
    /**
     * @return string
     */
    function getsnippetCode($domain)
    {
        return '
{%- if content_for_header contains "image-engine-io.js" -%}
  function updateSrcs(domain){
      let imgs = document.getElementsByTagName(\'img\');
      for( var i=0; i < imgs.length; i++ ){
          let src = imgs[i].src

          if( src != \'\' ){
              let oldHost = getHost(src);
              if( oldHost != \'\' && oldHost == \'cdn.shopify.com\'){
                  let newSrc = src.replace(oldHost, domain);
                  imgs[i].src = newSrc;
              }
          }
              let attr = [\'data-srcset\', \'srcset\', \'data-src\'];

              for( var j=0; j < attr.length; j++ ){
                  let dataSrcSet = imgs[i].getAttribute(attr[j]);
				  if( dataSrcSet != null ){
                    let dataSrcSetArr = dataSrcSet.split(", ");
                    dataSrcSetArr.forEach(function(e, index){
                        let u = e.replace(\' \', \'\', e);
                        let oldHost = getHost(u);
                        if( oldHost != \'\' && oldHost == \'cdn.shopify.com\'){
                            let newSrc = e.replace(oldHost, domain);
                            dataSrcSetArr[index] = newSrc;

                        }
                    });
                  	imgs[i].setAttribute(attr[j], dataSrcSetArr.join(\', \'));
                  }
              }
      }
  }

  function getHost(src){
      src = (src.indexOf("http") >= 0 || src.indexOf("https") >= 0) ? src : "https:" + src;
      if(is_url(src)){
          const url = new URL(src);
          return url.hostname;
      }else{
          console.log(src);
          return "";
      }
  }

  function is_url(str)
  {
      regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
      return regexp.test(str);
  }


window.onload = updateSrcs( \''.$domain.'\' );
{% endif %}
';
    }
}
