/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*****************************************!*\
  !*** ./resources/js/image-engine-io.js ***!
  \*****************************************/
// const host = process.env.MIX_APP_URL;
//
// const apiEndPoint = host + '/api';
//
// function init(){
//     let params = getParams('image-engine-io');
//     let shopifyDomain = params['shop'];
//     const Http = new XMLHttpRequest();
//     const sendurl = apiEndPoint + '/get-domain?shop=' + shopifyDomain;
//
//     const xhr = new XMLHttpRequest(),
//         method = "GET",
//         url = sendurl;
//
//     xhr.open(method, url, true);
//     xhr.onreadystatechange = function () {
//         // In local files, status is 0 upon success in Mozilla Firefox
//         if(xhr.readyState === XMLHttpRequest.DONE) {
//             var status = xhr.status;
//             if (status === 0 || (status >= 200 && status < 400)) {
//                 // The request has been completed successfully
//                 let res = JSON.parse(xhr.responseText);
//                 let data = res.data;
//                 if( res.isSuccess ){
//                     if( data != null ){
//                         let newDomain = data.domain;
//                         updateSrcs(newDomain);
//                     }else{
//                         console.log(data);
//                     }
//                 }else{
//                    console.log(data);
//                 }
//             } else {
//                 // Oh no! There has been an error with the request!
//             }
//         }
//     };
//     xhr.send();
// }
//
// function updateSrcs(domain){
//     let imgs = document.getElementsByTagName('img');
//     for( var i=0; i < imgs.length; i++ ){
//         let src = imgs[i].src
//
//         if( src != '' ){
//             let oldHost = getHost(src);
//             if( oldHost != '' && oldHost == 'cdn.shopify.com'){
//                 let newSrc = src.replace(oldHost, domain);
//                 imgs[i].src = newSrc;
//             }
//         }else{
//             let attr = ['data-srcset', 'srcset'];
//
//             for( var j=0; j < attr.length; j++ ){
//                 let dataSrcSet = imgs[i].getAttribute(attr[j]);
//
//                 let dataSrcSetArr = dataSrcSet.split(", ");
//                 if( dataSrcSet != null ){
//                     let dataSrcSetArr = dataSrcSet.split(", ");
//                     dataSrcSetArr.forEach(function(e, index){
//                     let u = e.replace(' ', '', e);
//                     let oldHost = getHost(u);
//                     if( oldHost != '' && oldHost == 'cdn.shopify.com'){
//                         let newSrc = e.replace(oldHost, domain);
//                         dataSrcSetArr[index] = newSrc;
//
//                     }
//                 });
//                  imgs[i].setAttribute(attr[j], dataSrcSetArr.join(', '));
//                  }
//             }
//         }
//     }
// }
//
// function getHost(src){
//     src = (src.indexOf("http") >= 0 || src.indexOf("https") >= 0) ? src : 'https:' + src;
//     if(is_url(src)){
//         const url = new URL(src);
//         return url.hostname;
//     }else{
//         console.log(src);
//         return '';
//     }
// }
//
// function is_url(str)
// {
//     regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
//     return regexp.test(str);
// }
//
//
// function getParams(script_name) {
//     // Find all script tags
//     var scripts = document.getElementsByTagName("script");
//     // Look through them trying to find ourselves
//     for (var i = 0; i < scripts.length; i++) {
//         if (scripts[i].src.indexOf("/" + script_name) > -1) {
//             // Get an array of key=value strings of params
//             var pa = scripts[i].src.split("?").pop().split("&");
//             // Split each key=value into array, the construct js object
//             var p = {};
//             for (var j = 0; j < pa.length; j++) {
//                 var kv = pa[j].split("=");
//                 p[kv[0]] = kv[1];
//             }
//             return p;
//         }
//     }
//
//     // No scripts match
//
//     return {};
// }
//
// window.onload = init();
/******/ })()
;