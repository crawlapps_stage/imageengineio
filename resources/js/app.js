require('./bootstrap');
import React, { Component, useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import {AppProvider} from "@shopify/polaris";
import en from '@shopify/polaris/locales/en.json';
import { Provider } from '@shopify/app-bridge-react';

import Domain from "./components/Pages/domain";
import Help from "./components/Pages/help";
import Settings from "./components/Pages/settings";

const config = window.shopify_app_bridge;

class App extends Component {
    constructor(props) {
        super(props);
        this.path = window.ImageEnginePath;
    }

    render() {
        switch(this.path) {
            case '/':
                return <Domain />
            case '/help':
                return <Help />
            case '/settings':
                return <Settings />
            default:
                return <Domain />
        }
    }
}
if (document.getElementById('app')) {
ReactDOM.render(
    <Provider
        config={config}
    >
    <AppProvider i18n={en} theme={{colorScheme: "light"}}>
        <App />
    </AppProvider>
    </Provider>,
    document.getElementById("app"));
}
