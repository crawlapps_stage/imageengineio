import React, {useCallback, useEffect, useState} from 'react';
import { useAppBridge, Toast } from '@shopify/app-bridge-react';
import {Button, Select} from '@shopify/polaris';
import {getSessionToken} from "@shopify/app-bridge-utils";

function Help() {
    const instance = axios.create();
    const app = useAppBridge();
    const [showToast, setShowToast] = useState(false);
    const toggleActive = useCallback(() => setShowToast((showToast) => !showToast), []);
    const toastMarkup = showToast ? (
        <Toast content="Saved" onDismiss={toggleActive} />
    ) : null;

    const [selected, setSelected] = useState(null);
    const handleSelectChange = useCallback((value) => setSelected(value.toString()), []);

    const [options, setOptions] = useState([]);
    function getInstance(){
        instance.interceptors.request.use(
            function (config) {
                return getSessionToken(app)  // requires an App Bridge instance
                    .then((token) => {
                        config.headers['Authorization'] = `Bearer ${token}`;
                        return config;
                    });
            });
    }

    useEffect(() => {
        // get data from serve of current plan to edit
        (async () => {
            getInstance();
            await instance.get('setting')
                .then(res => {
                    setOptions(res.data.data.theme);
                    setSelected(res.data.data.current.value);
                })
                .catch(err => {
                    console.log(err);
                })
        })();
    }, []);
    const saveSetting = useCallback(async ()  => {
        getInstance();
        await instance.post('setting', {data: selected})
            .then(res => {
                if (res.data.isSuccess) {
                    setShowToast(true);
                }
            })
            .catch(err => {
                console.log(err);
            })
    }, [selected]);

    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="setting_main">
                            <Select
                                label="Select Theme"
                                name="theme"
                                options={options}
                                onChange={handleSelectChange}
                                value={selected}
                            />

                            <div className='text-right mt-3 pt-4'>
                                <Button onClick={saveSetting} primary>Save</Button>
                                {toastMarkup}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Help;
