import React, {useCallback, useEffect, useState} from 'react';
import { Provider, TitleBar, useAppBridge } from '@shopify/app-bridge-react';
import {Toast} from '@shopify/app-bridge-react';
import {TextField, Button, InlineError, Page} from '@shopify/polaris';
import { getSessionToken } from "@shopify/app-bridge-utils";
function Domain() {
    const [data, setData] = useState({id: '', domain: ''});
    const [showToast, setShowToast] = useState(false);
    const [errors, setErrors] = useState([]);
    const app = useAppBridge();
    const toggleActive = useCallback(() => setShowToast((showToast) => !showToast), []);
    const toastMarkup = showToast ? (
        <Toast content="Saved" onDismiss={toggleActive} />
    ) : null;
    const instance = axios.create();
    function handleChange(value){
        setData({id: data.id, domain: value});
    }

    function getInstance(){
        instance.interceptors.request.use(
            function (config) {
                return getSessionToken(app)  // requires an App Bridge instance
                    .then((token) => {
                        config.headers['Authorization'] = `Bearer ${token}`;
                        return config;
                    });
            });
    }
    useEffect(() => {
        // get data from serve of current plan to edit
        (async () => {
            getInstance();
            await instance.get('domain')
                .then(res => {
                    setData(res.data.data);
                })
                .catch(err => {
                    console.log(err);
                })
        })();
    }, []);

    const saveDomain = useCallback(async ()  => {
        getInstance();
        await instance.post('domain', {data: data})
            .then(res => {
                if (res.data.isSuccess) {
                    setShowToast(true);
                }
            })
            .catch(err => {
                console.log(err);
            })
    }, [data]);

    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="domain_main">
                            <TextField label="Domain" value={data.domain} onChange={value => handleChange(value)} placeholder="xyz.myshopify.com"/>
                            <div className='text-right mt-3 pt-4'>
                                <Button onClick={saveDomain} primary>Save</Button>
                                {toastMarkup}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Domain;

