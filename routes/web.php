<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Domain\DomainController;
use App\Http\Controllers\Setting\SettingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
    '/login', [AuthController::class,'index']
)->name('login');

Route::group(['middleware' => ['auth.shopify']], function () {
    Route::get('/', [DomainController::class, 'component'])->name('home');
    Route::get('/help', [DomainController::class, 'component']);
    Route::get('/settings', [DomainController::class, 'component']);
});

Route::group(['middleware' => ['shopifysession.auth']], function () {
    Route::resource('domain', DomainController::class);

    //settings
    Route::resource('setting', SettingController::class);
});

Route::get('flush', function(){
    request()->session()->flush();
});
